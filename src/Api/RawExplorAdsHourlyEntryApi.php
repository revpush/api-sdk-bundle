<?php
/**
 * RawExplorAdsHourlyEntryApi
 * PHP version 7.4
 *
 * @category Class
 * @package  Swagger\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * 
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * Generator version: 7.7.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Swagger\Client\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Swagger\Client\ApiException;
use Swagger\Client\Configuration;
use Swagger\Client\HeaderSelector;
use Swagger\Client\ObjectSerializer;

/**
 * RawExplorAdsHourlyEntryApi Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class RawExplorAdsHourlyEntryApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var HeaderSelector
     */
    protected $headerSelector;

    /**
     * @var int Host index
     */
    protected $hostIndex;

    /** @var string[] $contentTypes **/
    public const contentTypes = [
        'getRawExplorAdsHourlyEntryCollection' => [
            'application/json',
        ],
    ];

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     * @param int             $hostIndex (Optional) host index to select the list of hosts if defined in the OpenAPI spec
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null,
        $hostIndex = 0
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
        $this->hostIndex = $hostIndex;
    }

    /**
     * Set the host index
     *
     * @param int $hostIndex Host index (required)
     */
    public function setHostIndex($hostIndex): void
    {
        $this->hostIndex = $hostIndex;
    }

    /**
     * Get the host index
     *
     * @return int Host index
     */
    public function getHostIndex()
    {
        return $this->hostIndex;
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation getRawExplorAdsHourlyEntryCollection
     *
     * Retrieves the collection of RawExplorAdsHourlyEntry resources.
     *
     * @param  string $date  (required)
     * @param  int $hour  (required)
     * @param  int $page The collection page number (optional, default to 1)
     * @param  int $limit The number of items per page (optional, default to 30)
     * @param  string $contentType The value for the Content-Type header. Check self::contentTypes['getRawExplorAdsHourlyEntryCollection'] to see the possible values for this operation
     *
     * @throws \Swagger\Client\ApiException on non-2xx response or if the response body is not in the expected format
     * @throws \InvalidArgumentException
     * @return \Swagger\Client\Model\RawExplorAdsHourlyEntryRawExplorAdsHourlyEntryRead[]
     */
    public function getRawExplorAdsHourlyEntryCollection($date, $hour, $page = 1, $limit = 30, string $contentType = self::contentTypes['getRawExplorAdsHourlyEntryCollection'][0])
    {
        list($response) = $this->getRawExplorAdsHourlyEntryCollectionWithHttpInfo($date, $hour, $page, $limit, $contentType);
        return $response;
    }

    /**
     * Operation getRawExplorAdsHourlyEntryCollectionWithHttpInfo
     *
     * Retrieves the collection of RawExplorAdsHourlyEntry resources.
     *
     * @param  string $date  (required)
     * @param  int $hour  (required)
     * @param  int $page The collection page number (optional, default to 1)
     * @param  int $limit The number of items per page (optional, default to 30)
     * @param  string $contentType The value for the Content-Type header. Check self::contentTypes['getRawExplorAdsHourlyEntryCollection'] to see the possible values for this operation
     *
     * @throws \Swagger\Client\ApiException on non-2xx response or if the response body is not in the expected format
     * @throws \InvalidArgumentException
     * @return array of \Swagger\Client\Model\RawExplorAdsHourlyEntryRawExplorAdsHourlyEntryRead[], HTTP status code, HTTP response headers (array of strings)
     */
    public function getRawExplorAdsHourlyEntryCollectionWithHttpInfo($date, $hour, $page = 1, $limit = 30, string $contentType = self::contentTypes['getRawExplorAdsHourlyEntryCollection'][0])
    {
        $request = $this->getRawExplorAdsHourlyEntryCollectionRequest($date, $hour, $page, $limit, $contentType);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    (int) $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? (string) $e->getResponse()->getBody() : null
                );
            } catch (ConnectException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    (int) $e->getCode(),
                    null,
                    null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        (string) $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    (string) $response->getBody()
                );
            }

            switch($statusCode) {
                case 200:
                    if ('\Swagger\Client\Model\RawExplorAdsHourlyEntryRawExplorAdsHourlyEntryRead[]' === '\SplFileObject') {
                        $content = $response->getBody(); //stream goes to serializer
                    } else {
                        $content = (string) $response->getBody();
                        if ('\Swagger\Client\Model\RawExplorAdsHourlyEntryRawExplorAdsHourlyEntryRead[]' !== 'string') {
                            try {
                                $content = json_decode($content, false, 512, JSON_THROW_ON_ERROR);
                            } catch (\JsonException $exception) {
                                throw new ApiException(
                                    sprintf(
                                        'Error JSON decoding server response (%s)',
                                        $request->getUri()
                                    ),
                                    $statusCode,
                                    $response->getHeaders(),
                                    $content
                                );
                            }
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, '\Swagger\Client\Model\RawExplorAdsHourlyEntryRawExplorAdsHourlyEntryRead[]', []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
            }

            $returnType = '\Swagger\Client\Model\RawExplorAdsHourlyEntryRawExplorAdsHourlyEntryRead[]';
            if ($returnType === '\SplFileObject') {
                $content = $response->getBody(); //stream goes to serializer
            } else {
                $content = (string) $response->getBody();
                if ($returnType !== 'string') {
                    try {
                        $content = json_decode($content, false, 512, JSON_THROW_ON_ERROR);
                    } catch (\JsonException $exception) {
                        throw new ApiException(
                            sprintf(
                                'Error JSON decoding server response (%s)',
                                $request->getUri()
                            ),
                            $statusCode,
                            $response->getHeaders(),
                            $content
                        );
                    }
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\RawExplorAdsHourlyEntryRawExplorAdsHourlyEntryRead[]',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getRawExplorAdsHourlyEntryCollectionAsync
     *
     * Retrieves the collection of RawExplorAdsHourlyEntry resources.
     *
     * @param  string $date  (required)
     * @param  int $hour  (required)
     * @param  int $page The collection page number (optional, default to 1)
     * @param  int $limit The number of items per page (optional, default to 30)
     * @param  string $contentType The value for the Content-Type header. Check self::contentTypes['getRawExplorAdsHourlyEntryCollection'] to see the possible values for this operation
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getRawExplorAdsHourlyEntryCollectionAsync($date, $hour, $page = 1, $limit = 30, string $contentType = self::contentTypes['getRawExplorAdsHourlyEntryCollection'][0])
    {
        return $this->getRawExplorAdsHourlyEntryCollectionAsyncWithHttpInfo($date, $hour, $page, $limit, $contentType)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getRawExplorAdsHourlyEntryCollectionAsyncWithHttpInfo
     *
     * Retrieves the collection of RawExplorAdsHourlyEntry resources.
     *
     * @param  string $date  (required)
     * @param  int $hour  (required)
     * @param  int $page The collection page number (optional, default to 1)
     * @param  int $limit The number of items per page (optional, default to 30)
     * @param  string $contentType The value for the Content-Type header. Check self::contentTypes['getRawExplorAdsHourlyEntryCollection'] to see the possible values for this operation
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getRawExplorAdsHourlyEntryCollectionAsyncWithHttpInfo($date, $hour, $page = 1, $limit = 30, string $contentType = self::contentTypes['getRawExplorAdsHourlyEntryCollection'][0])
    {
        $returnType = '\Swagger\Client\Model\RawExplorAdsHourlyEntryRawExplorAdsHourlyEntryRead[]';
        $request = $this->getRawExplorAdsHourlyEntryCollectionRequest($date, $hour, $page, $limit, $contentType);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    if ($returnType === '\SplFileObject') {
                        $content = $response->getBody(); //stream goes to serializer
                    } else {
                        $content = (string) $response->getBody();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        (string) $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getRawExplorAdsHourlyEntryCollection'
     *
     * @param  string $date  (required)
     * @param  int $hour  (required)
     * @param  int $page The collection page number (optional, default to 1)
     * @param  int $limit The number of items per page (optional, default to 30)
     * @param  string $contentType The value for the Content-Type header. Check self::contentTypes['getRawExplorAdsHourlyEntryCollection'] to see the possible values for this operation
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    public function getRawExplorAdsHourlyEntryCollectionRequest($date, $hour, $page = 1, $limit = 30, string $contentType = self::contentTypes['getRawExplorAdsHourlyEntryCollection'][0])
    {

        // verify the required parameter 'date' is set
        if ($date === null || (is_array($date) && count($date) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $date when calling getRawExplorAdsHourlyEntryCollection'
            );
        }

        // verify the required parameter 'hour' is set
        if ($hour === null || (is_array($hour) && count($hour) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $hour when calling getRawExplorAdsHourlyEntryCollection'
            );
        }


        if ($limit !== null && $limit < 0) {
            throw new \InvalidArgumentException('invalid value for "$limit" when calling RawExplorAdsHourlyEntryApi.getRawExplorAdsHourlyEntryCollection, must be bigger than or equal to 0.');
        }
        

        $resourcePath = '/api/raw_explor_ads_hourly_entries';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        $queryParams = array_merge($queryParams, ObjectSerializer::toQueryValue(
            $page,
            'page', // param base name
            'integer', // openApiType
            'form', // style
            false, // explode
            false // required
        ) ?? []);
        // query params
        $queryParams = array_merge($queryParams, ObjectSerializer::toQueryValue(
            $limit,
            'limit', // param base name
            'integer', // openApiType
            'form', // style
            false, // explode
            false // required
        ) ?? []);
        // query params
        $queryParams = array_merge($queryParams, ObjectSerializer::toQueryValue(
            $date,
            'date', // param base name
            'string', // openApiType
            'form', // style
            false, // explode
            true // required
        ) ?? []);
        // query params
        $queryParams = array_merge($queryParams, ObjectSerializer::toQueryValue(
            $hour,
            'hour', // param base name
            'integer', // openApiType
            'form', // style
            false, // explode
            true // required
        ) ?? []);




        $headers = $this->headerSelector->selectHeaders(
            ['application/json', 'text/html', ],
            $contentType,
            $multipart
        );

        // for model (json/xml)
        if (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $formParamValueItems = is_array($formParamValue) ? $formParamValue : [$formParamValue];
                    foreach ($formParamValueItems as $formParamValueItem) {
                        $multipartContents[] = [
                            'name' => $formParamName,
                            'contents' => $formParamValueItem
                        ];
                    }
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif (stripos($headers['Content-Type'], 'application/json') !== false) {
                # if Content-Type contains "application/json", json_encode the form parameters
                $httpBody = \GuzzleHttp\Utils::jsonEncode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = ObjectSerializer::buildQuery($formParams);
            }
        }

        // this endpoint requires API key authentication
        $apiKey = $this->config->getApiKeyWithPrefix('X-AUTH-TOKEN');
        if ($apiKey !== null) {
            $headers['X-AUTH-TOKEN'] = $apiKey;
        }

        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $operationHost = $this->config->getHost();
        $query = ObjectSerializer::buildQuery($queryParams);
        return new Request(
            'GET',
            $operationHost . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Create http client option
     *
     * @throws \RuntimeException on file opening failure
     * @return array of http client options
     */
    protected function createHttpClientOption()
    {
        $options = [];
        if ($this->config->getDebug()) {
            $options[RequestOptions::DEBUG] = fopen($this->config->getDebugFile(), 'a');
            if (!$options[RequestOptions::DEBUG]) {
                throw new \RuntimeException('Failed to open the debug file: ' . $this->config->getDebugFile());
            }
        }

        return $options;
    }
}
